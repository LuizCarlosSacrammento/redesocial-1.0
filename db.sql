-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 13-Jun-2016 às 05:52
-- Versão do servidor: 5.7.12-log
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `busca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(20) NOT NULL,
  `id_post` int(100) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `comentario` text NOT NULL,
  `data` varchar(200) NOT NULL,
  `hora` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`id`, `id_post`, `nome`, `comentario`, `data`, `hora`) VALUES
(1, 5, 'fasfsa', 'fasfa', '10/06/2016', '21:34'),
(2, 5, 'Thiago Sales', 'Estou comentando aqui', '10/06/2016', '21:44'),
(3, 4, 'Gustavo', 'QUalquer coisa', '10/06/2016', '21:45'),
(4, 4, 'fasfa', 'fasfsafa', '10/06/2016', '21:47'),
(5, 5, 'fsafaf', 'fasfsafsaf', '11/06/2016', '02:39'),
(6, 8, 'fasf', 'fasfsafsa', '11/06/2016', '03:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curtidas`
--

CREATE TABLE `curtidas` (
  `id` int(20) NOT NULL,
  `id_post` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `curtidas`
--

INSERT INTO `curtidas` (`id`, `id_post`) VALUES
(1, 5),
(2, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE `posts` (
  `id` int(20) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descricao` text NOT NULL,
  `imagem` varchar(200) NOT NULL,
  `data` varchar(200) NOT NULL,
  `hora` varchar(200) NOT NULL,
  `postador` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `titulo`, `descricao`, `imagem`, `data`, `hora`, `postador`) VALUES
(1, 'Primeira postagem', 'Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa Qualquer coisa', '', '09/06/2016', '12:26', 'thiagoSales'),
(2, 'Minha segunda postagem', '', 'http://i.imgur.com/o6DmUif.jpg', '09/06/2016', '12:27', 'thiagoSales'),
(3, 'fsafas', 'descricao', 'Parte3.png', '10/06/2016', '02:32:45', 'thiagoSales'),
(4, 'fasf', 'descricao', 'imagens/uploads/Parte3.png', '10/06/2016', '02:33:26', 'fasf'),
(5, 'Minha publicação', 'descricao', 'imagens/uploads/Parte3.png', '10/06/2016', '02:36:00', 'thiagoSales'),
(8, 'Vídeo novo galera', '<iframe width="100%" height="315" src="https://www.youtube.com/embed/iMmuw35bXe4" frameborder="0" allowfullscreen></iframe>', '', '11/06/2016', '03:08:31', 'thiagoSales'),
(9, 'Texto sem foto', 'fmaslfjsalfkjsalfsa', '', '11/06/2016', '03:38:00', 'thiagoSales'),
(10, 'SISTEMA DE POSTAGEM / PUBLICAÇÃO PHP + MYSQL #5 - CURTIR + CORREÇÕES ', '<iframe width="100%" height="315" src="https://www.youtube.com/embed/kenWnVHdl7g" frameborder="0" allowfullscreen></iframe>', '', '12/06/2016', '00:18:12', 'thiagoSales'),
(11, 'dasd', 'sadsad', '', '12/06/2016', '01:01:09', 'dsad'),
(12, 'Ele... :)', '<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Ffantasticafabricadedesocupados%2Fposts%2F1408838015812671%3A0&width=500" width="500" height="565" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>', '', '12/06/2016', '10:26:13', 'thiagoSales'),
(13, 'haha', '<blockquote class="twitter-tweet" data-lang="pt"><p lang="pt" dir="ltr">nois queria ta como / como nois ta <a href="https://t.co/y310xn96aq">pic.twitter.com/y310xn96aq</a></p>&mdash; Smile (@smiIle) <a href="https://twitter.com/smiIle/status/741360913417129984">10 de junho de 2016</a></blockquote>\r\n<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>', '', '12/06/2016', '10:33:29', 'thiagoSales'),
(14, 'Testando Tweet', '<blockquote class="twitter-tweet" data-lang="pt" width="100%"><p lang="pt" dir="ltr">Ganhe o Camisola oficial de la SELEÇÃO! <a href="https://t.co/7lJUXqraLE">https://t.co/7lJUXqraLE</a></p>&mdash; O Cantinho Legal (@oCantinhoLegal) <a href="https://twitter.com/oCantinhoLegal/status/737654231105208320">31 de maio de 2016</a></blockquote>\r\n<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>', '', '12/06/2016', '21:52:28', 'thiagoSales');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `nome` varchar(200) NOT NULL,
  `usuario` varchar(200) NOT NULL,
  `senha` varchar(200) NOT NULL,
  `foto` varchar(200) NOT NULL DEFAULT '../imagens/nophoto.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`nome`, `usuario`, `senha`, `foto`) VALUES
('Thiago Sales', 'thiago5', 'thiago5', '../imagens/usuarios/10734101_725668044188585_6704974216755808212_n.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curtidas`
--
ALTER TABLE `curtidas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `curtidas`
--
ALTER TABLE `curtidas`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
