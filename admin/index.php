<?php
	include_once("settings/settings.php");
	@session_start();

?>

<html>
	<head>
		<title>Poesia e Filosofia</title>
		<link rel="stylesheet" href="css/style.css" type="text/css">

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	</head>

	<body>

	<?php if(isset($_SESSION['nome'])  && isset($_SESSION['usuario'])){?>
	<div class="col-xs-4">
		<nav class="navbar navbar-default">
			<p><img src="<?php echo $_SESSION['foto'];?>" class="foto-user"/> <b><?php echo $_SESSION['nome'];?></b></p>
			<div class="list-group">
				<a href="?pagina=inicio" class="list-group-item"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicio</a>
				<!--<a href="?pagina=publicar" class="list-group-item"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Publicar</a>-->
				<a href="?pagina=post" class="list-group-item"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Postagens</a>
			<!--<a href="?pagina=usuarios" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Usuários</a>>-->
				<!--<a href="?pagina=cadastro" class="list-group-item"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Cadastrar usuários</a>
-->				<a href="?pagina=editarperfil" class="list-group-item"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Editar perfil</a>
				<a href="?pagina=sair" class="list-group-item"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Sair</a>
			</div>
		</nav>
	</div><?}?>
		<div <? if(isset($_SESSION['nome'])){?> class="col-xs-6"<?}?>>
			<?php
				if(isset($_GET['pagina'])){
					$do = ($_GET['pagina']);
				}else{
					$do = "inicio";
				}

				if(file_exists("paginas/".$do.".php")){
					include("paginas/".$do.".php");
				}else{
					print 'Página não encontrada!';
				}
			?>
		</div>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>
</html>
